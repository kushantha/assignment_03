#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num;
    printf("Enter a new number: ");
    scanf("%d", &num);
    printf("\n");
    if (num > 0)
    {
        printf("A positive number\n");
    }
    else if (num < 0)
    {
        printf("A negative number\n");
    }
    else
    {
        printf("It is zero\n");
    }
    return 0;
}
