#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num,reminder;
    printf("Enter a new number: ");
    scanf("%d", &num);
    printf("\n");
    reminder = num%2;

    if (reminder == 1)
    {
        printf("A odd number\n");
    }
    else
    {
        printf("A even number\n");
    }
    return 0;
}
