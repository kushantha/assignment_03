#include <stdio.h>
#include <stdlib.h>

int main()
{
    char letter;
    printf("Enter the letter : ");
    scanf("%char", &letter);
    printf("\n");
    if (letter == 'a'||letter == 'A'||letter == 'e'||letter == 'E'||letter == 'i'||letter == 'I'||letter == 'o'||letter == 'O'||letter == 'u'||letter == 'U')
    {
        printf("This is a vowel");
    }
    else{
        printf("This is a consonant");
    }
    return 0;
}
